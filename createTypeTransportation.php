<?php

include_once ('core.php');

include_once ('adminHeader.php');

?>



<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Add Officer</div>

					<div class="card-body">
						<form method="POST" action="addTypeTransportation.php">
							<div class="form-group row">
								<label for="email" class="col-md-4 col-form-label text-md-right">Name</label>

								<div class="col-md-6">
									<input id="email" type="text"  name="nama_type" required autofocus>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Note</label>

								<div class="col-md-6">
									<input id="password" type="text" name="keterangan" required>
								</div>
							</div>

							<div class="form-group row mb-0">
								<div class="col-md-8 offset-md-4">
									<button type="submit" class="btn btn-primary">
										Add
									</button>
								</div>
							</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<?php

include_once ('adminFooter.php');

?>