<?php

include_once ('core.php');

include_once ('adminHeader.php');

?>



<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Add Rute</div>

					<div class="card-body">
						<form method="POST" action="addRute.php">
							<div class="form-group row">
								<label for="email" class="col-md-4 col-form-label text-md-right">Destination</label>

								<div class="col-md-6">
									<input id="email" type="text"  name="tujuan" required autofocus>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">First Rute</label>

								<div class="col-md-6">
									<input id="password" type="text" name="rute_awal" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Last Rute</label>

								<div class="col-md-6">
									<input id="password" type="text" name="rute_akhir" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Price</label>

								<div class="col-md-6">
									<input id="password" type="number" name="harga" required>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Transportation</label>

								<div class="col-md-6">
									<Select name="id_transportasi">
										<?php
										$query = "SELECT * FROM transportasi";
										$result = mysqli_query($con, $query); 
										if (mysqli_num_rows($result) > 0) {
											while($row = mysqli_fetch_assoc($result)) {
										?>
										<option value="<?php echo $row['id_transportasi']; ?>"><?php echo $row['keterangan']; ?></option>
										<?php
											}
										}
										?>
									</Select>
								</div>
							</div>


							<div class="form-group row mb-0">
								<div class="col-md-8 offset-md-4">
									<button type="submit" class="btn btn-primary">
										Add
									</button>
								</div>
							</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<?php

include_once ('adminFooter.php');

?>