<?php

include_once ('core.php');

$username = $_POST['username'];
$password = $_POST['password'];

$login = loginOfficer($username, $password);

if ($login) {
    if($_SESSION['status'] == 'administrator') {
        header("Location: homeAdmin.php");

    } else {
        header("Location: homeOfficer.php");

    }
} else {
    header("Location: index.php");
}

?>