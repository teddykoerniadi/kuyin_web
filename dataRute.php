<?php

include_once ('core.php');

include_once ('adminHeader.php');

?>

<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">Rute</div>

					<div class="card-body">
						<a href="createRute.php" class="btn btn-primary">Add Data</a>

                        <br>
                        <br>

                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Destination</th>
                                <th scope="col">First Rute</th>
                                <th scope="col">Last Rute</th>
                                <th scope="col">Price</th>
                                <th scope="col">Id Transportation</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                            <?php
                            $query = "SELECT * FROM rute";
                            $result = mysqli_query($con, $query);
                            if (mysqli_num_rows($result) > 0) {
                                while($row = mysqli_fetch_assoc($result)) {
                                    ?>
                                        <tr>
                                        <th scope="row"><?php echo $row['id_rute']; ?></th>
                                        <td><?php echo $row['tujuan']; ?></td>
                                        <td><?php echo $row['rute_awal']; ?></td>
                                        <td><?php echo $row['rute_akhir']; ?></td>
                                        <td><?php echo $row['harga']; ?></td>
                                        <td><?php echo $row['id_transportasi']; ?></td>
                                        <td>
                                            <form action="deleteRute.php" method="POST">
                                                <input type="hidden" name="id_rute" value="<?php echo $row['id_rute']; ?>">
                                                <input type="submit" value="Delete" class="btn btn-danger">
                                            </form>
                                        </td>
                                        </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                            </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<?php

include_once ('adminFooter.php');

?>