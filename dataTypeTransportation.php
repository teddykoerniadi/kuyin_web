<?php

include_once ('core.php');

include_once ('adminHeader.php');

?>

<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">Type Transportation</div>

					<div class="card-body">
						<a href="createTypeTransportation.php" class="btn btn-primary">Add Data</a>

                        <br>
                        <br>

                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Note</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                            <?php
                            $query = "SELECT * FROM type_transportasi";
                            $result = mysqli_query($con, $query);
                            if (mysqli_num_rows($result) > 0) {
                                while($row = mysqli_fetch_assoc($result)) {
                                    ?>
                                        <tr>
                                        <th scope="row"><?php echo $row['id_type_transportasi']; ?></th>
                                        <td><?php echo $row['nama_type']; ?></td>
                                        <td><?php echo $row['keterangan']; ?></td>
                                        <td>
                                            <form action="deleteTypeTransportation.php" method="POST">
                                                <input type="hidden" name="id_type_transportation" value="<?php echo $row['id_type_transportasi']; ?>">
                                                <input type="submit" value="Delete" class="btn btn-danger">
                                            </form>
                                        </td>
                                        </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                            </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<?php

include_once ('adminFooter.php');

?>