<?php

include_once ('core.php');

include_once ('adminHeader.php');

$kode = substr(md5(microtime()),rand(0,26),10);
?>



<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Add Order</div>

					<div class="card-body">
						<form method="POST" action="addOrder.php">
							<input type="hidden" name="kode_pemesanan" value="<?php echo $kode; ?>">
							<input type="hidden" name="id_pelanggan" value="<?php echo $_SESSION['id']; ?>">

							<div class="form-group row">
								<label for="email" class="col-md-4 col-form-label text-md-right">Place Order</label>

								<div class="col-md-6">
									<input id="email" type="text"  name="tempat_pemesanan" required autofocus>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Chair Code</label>

								<div class="col-md-6">
									<input id="password" type="number" name="kode_kursi" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Destination</label>

								<div class="col-md-6">
									<Select name="id_rute">
										<?php
										$query = "SELECT * FROM rute";
										$result = mysqli_query($con, $query); 
										if (mysqli_num_rows($result) > 0) {
											while($row = mysqli_fetch_assoc($result)) {
										?>
										<option value="<?php echo $row['id_rute']; ?>"><?php echo $row['tujuan']; ?></option>
										<?php
											}
										}
										?>
									</Select>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Date Berangkat</label>

								<div class="col-md-6">
									<input id="password" type="date" name="tanggal_berangkat" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Time Check In</label>

								<div class="col-md-6">
									<input id="password" type="time" name="jam_cekin" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Time Berangkat</label>

								<div class="col-md-6">
									<input id="password" type="time" name="jam_berangkat" required>
								</div>
							</div>


							<div class="form-group row mb-0">
								<div class="col-md-8 offset-md-4">
									<button type="submit" class="btn btn-primary">
										Add
									</button>
								</div>
							</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<?php

include_once ('adminFooter.php');

?>