<?php

include_once ('core.php');

include_once ('adminHeader.php');

?>

<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Welcome</div>

					<div class="card-body">
						Welcome <?php  echo $_SESSION['nama_petugas'] ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php

include_once ('adminFooter.php');

?>