<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Kuyin</title>

    <!-- Scripts -->
    <script src="./resources/js/jquery-3.3.1.slim.min.js" defer></script>
    <script src="./resources/js/popper.min.js" defer></script>
    <script src="./resources/js/bootstrap.min.js" defer></script>

    <!-- Fonts -->

    <!-- Styles -->
    <link href="./resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="./resources/css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="#">
                    Kuyin
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
						<li class="nav-item">
							<a class="nav-link" href="index.php">Login</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="createCustomer.php">Register</a>
						</li>
                    </ul>
                </div>
            </div>
        </nav>